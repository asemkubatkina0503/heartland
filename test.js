// Импортируем необходимые модули из WebDriverIO
const { remote } = require('webdriverio');
const assert = require('assert');

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}



(async () => {

  const chromeOptions = {
    args: ['--start-maximized']
  };


  const browser = await remote({
    capabilities: {
      browserName: 'chrome',
      'goog:chromeOptions': chromeOptions,
    },
  });


  await browser.url("https://magento.softwaretestingboard.com/");
  assert(await browser.getUrl() === 'https://magento.softwaretestingboard.com/', 'The expected URL is not opened');

  await browser.$("//*[@id='ui-id-2']/li[2]").click();
  assert(await browser.getUrl() === 'https://magento.softwaretestingboard.com/women.html', 'The expected URL is not opened');

  await browser.$(".categories-menu").$("//*[@class='items']/li[2]/a").click();
  assert(await browser.getUrl() === 'https://magento.softwaretestingboard.com/women/tops-women/jackets-women.html', 'The expected URL is not opened');

  await browser.$(".product-items").$(".product-item-info").click();
  assert(await browser.getUrl() === 'https://magento.softwaretestingboard.com/olivia-1-4-zip-light-jacket.html', 'The expected URL is not opened');

  await browser.$("#option-label-size-143-item-166").click()
  assert(await browser.$("#option-label-size-143-item-166").getAttribute('aria-checked') === 'true');

  await browser.$("#option-label-color-93-item-49").click();
  assert(await browser.$("#option-label-color-93-item-49").getAttribute('aria-checked') === 'true');

  await browser.$("#qty").doubleClick();
  await browser.$("#qty").setValue("3");

  await browser.$("aria/Add to Cart").click();

  await sleep(5000);

  await browser.$('.showcart').$('.counter').click();
  await browser.$("#top-cart-btn-checkout").click();
  assert(await browser.getUrl() === 'https://magento.softwaretestingboard.com/checkout/');

  await sleep(10000);
  
  assert(await browser.getUrl() === 'https://magento.softwaretestingboard.com/checkout/#shipping');
  await browser.$('#customer-email').click();
  await browser.$('#customer-email').setValue("test12333123@test.test");

  await browser.$("[name='shippingAddress.firstname'] > div").click();
  await browser.$("[name='shippingAddress.firstname'] > div").setValue("firstname");

  await browser.$("[name='shippingAddress.lastname'] > div").click();
  await browser.$("[name='shippingAddress.lastname'] > div").setValue("lastname");

  await browser.$("[name='shippingAddress.street.0'] > div").click();
  await browser.$("[name='shippingAddress.street.0'] > div").setValue("street");

  await browser.$("[name='shippingAddress.city'] > div").click();
  await browser.$("[name='shippingAddress.city'] > div").setValue("city");

  await browser.$("[name='shippingAddress.region_id'] > div").click();
  await sleep(1000);
  await browser.$("//*[@name='shippingAddress.region_id']/div/select/option[5]").click()

  await browser.$("[name='shippingAddress.postcode'] > div").click();
  await browser.$("[name='shippingAddress.postcode'] > div").setValue("12345");
  await sleep(3000);

  await browser.$("[name='shippingAddress.telephone'] > div").click();
  await browser.$("[name='shippingAddress.telephone'] > div").setValue("+9953333333333");
  await sleep(1000);

  await browser.$('.table-checkout-shipping-method').$('.radio').click();
  await sleep(3000);

  await browser.$("//*[@id='shipping-method-buttons-container']/div/button").click();
  await sleep(5000);
  assert(await browser.getUrl() === 'https://magento.softwaretestingboard.com/checkout/#payment');
  

  await browser.$("aria/Place Order").click()
 

  await sleep(10000);
  assert(await browser.getUrl() === 'https://magento.softwaretestingboard.com/checkout/onepage/success/');
  

  
  await browser.deleteSession();

})();